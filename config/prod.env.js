'use strict'
module.exports = {
  NODE_ENV: '"production"',
  // API: '"https://203.150.37.131/api"',
  // LINK: '"https://203.150.37.131"',
  // DOMAIN: '"https://203.150.37.131"'
  // API: '"http://surveys.inet.co.th:7000"',
  // LINK: '"http://surveys.inet.co.th:82"',
  // DOMAIN: '"inet.co.th"'

  API: '"https://customersurvey.inet.co.th/api"',
  LINK: '"https://customersurvey.inet.co.th"',
  DOMAIN: '"customersurvey.inet.co.th"'
}
