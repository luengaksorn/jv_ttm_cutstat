'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',

  // API: '"http://192.168.75.231:8000"',
  API: '"http://mean-serve.xyz:8000"',
  
  LINK: '"http://localhost:8080"',
  DOMAIN: '"localhost"'

  // API: '"http://203.150.243.178:8000"',
  // LINK: '"http://localhost:8080"',
  // DOMAIN: '"localhost"'
})
