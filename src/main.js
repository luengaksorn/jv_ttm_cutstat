// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import '../static/fonts/Athiti/stylesheet.css'
import App from './App'
import router from './router'
import VueSweetalert2 from 'vue-sweetalert2'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueCookies from 'vue-cookies'
import { VueSpinners } from '@saeris/vue-spinners'
import VueScrollProgressBar from '@guillaumebriday/vue-scroll-progress-bar'
import '@mdi/font/css/materialdesignicons.css'
import SmoothScrollbar from 'vue-smooth-scrollbar'
import Vuex from 'vuex'
import VueClipboard from 'vue-clipboard2'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
// import store from '@/components/store/store.js'
export const bus = new Vue()

Vue.use(Vuetify, {
  iconfont: 'mdi'
})
// Vue.use(Vuetify)
Vue.use(Vuex)
Vue.use(VueSweetalert2)
Vue.use(VueAxios, axios)
Vue.use(VueCookies)
Vue.use(VueSpinners)
Vue.use(VueScrollProgressBar)
Vue.use(SmoothScrollbar)
Vue.use(VueClipboard)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  // store,
  components: { App },
  template: '<App/>'
})
