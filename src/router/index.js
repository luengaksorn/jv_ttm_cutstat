import Vue from 'vue'
import Router from 'vue-router'
import navbar from '@/components/navbar'
import CustomerPage from '@/components/Customer/CustomerPage'
import DashboardSass from '@/components/DashboardSass/SassPage'
import DashboardSoftwarepartner from '@/components/DashboardSoftwarepartner/SoftwarepartnerPage'
import Dashbordinetms from '@/components/Dashbordinetms/inetmsPage'
import NewDashbordinetms from '@/components/DashbordNewinetms/NewinetmsPage'
import SatisfactionSaaS from '@/components/Sass/SatisfactionSaaS'
import SoftwarePartner from '@/components/SoftwarePartner/SoftwarePartner'
// import ViewsatSaas from '@/components/DashboardSass/ViewsatSaas'
import inetms from '@/components/inetms/inetms'
import Newinetms from '@/components/NewVesionInetms/Newinetms'
import SuccessPage from '@/components/SuccessPage'
import Login from '@/components/login'
// import Test from '@/components/test'
import Questionnaire from '@/components/DashboardQuestionnaire/QuestionnairePage'
import SatisfactionQuestionaire from '@/components/Questionnaire/SatisfactionQuestionaire'
import SatisfactionTalktome from '@/components/Talktome/SatisfactionTalktome'
import Talktome from '@/components/DashboardTalktome/TalktomePage'
//  import insertSatisfactionQuestionaire from '@/components/insertQuestionnaire/insertSatisfactionQuestionaire'
import ReportExcelQuestionnaire from '@/components/DashboardReportExcelQuestionnaire/QuestionnaireReportPage'
import ReportExcelTalktome from '@/components/DashboardReportExcelTalktome/TalktomeReportPage'
import UserManagement from '@/components/UserManagement/UserManagement'
import BackupCustomerDatabase from '@/components/BackupCustomerDatabase/CustomerDatabase'
import BackupINETCustSat from '@/components/BackupINETCustSat/INETCustSat'
import BackupINETFollowUp from '@/components/BackupINETFollowUp/INETFollowUp'
import Page404 from '@/components/Page404'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    { path: '/backoffice', component: navbar, children:
      [{ path: '/backoffice/Customer', component: CustomerPage },
        { path: '/backoffice/DashboardSass', component: DashboardSass },

        { path: '/backoffice/INETCust.Sat.', component: Questionnaire },
        { path: '/backoffice/INETFollowUp', component: Talktome },

        { path: '/backoffice/ExportINETCust.Sat.', component: ReportExcelQuestionnaire },
        { path: '/backoffice/ExportINETFollowUp', component: ReportExcelTalktome },
        { path: '/backoffice/UserManagement', component: UserManagement },
        { path: '/backoffice/BackupCustomerDatabase', component: BackupCustomerDatabase },
        { path: '/backoffice/BackupINETCustSat', component: BackupINETCustSat },
        { path: '/backoffice/BackupINETFollowUp', component: BackupINETFollowUp },

        { path: '/backoffice/DashboardSoftwarepartner', component: DashboardSoftwarepartner },
        { path: '/backoffice/Dashbordinetms', component: Dashbordinetms },
        { path: '/backoffice/NewDashbordinetms', component: NewDashbordinetms }]
    },
    { path: '/backoffice', component: navbar, children: [] },

    { path: '/login', component: Login },
    { path: '/CusSaas/:cusid', component: SatisfactionSaaS },
    { path: '/CusSoftware/:cusid', component: SoftwarePartner },
    { path: '/inetms/:cusid', component: inetms },
    { path: '/Newinetms/:cusid', component: Newinetms },
    { path: '/Success', component: SuccessPage },
    { path: '/Page404', component: Page404},
    { path: '/ViewsatSaas/:version/:mode/:cusid', component: SatisfactionSaaS },
    { path: '/EditsatSaas/:version/:mode/:cusid', component: SatisfactionSaaS },
    { path: '/ViewsatSoftware/:version/:mode/:cusid', component: SoftwarePartner },
    { path: '/EditsatSoftware/:version/:mode/:cusid', component: SoftwarePartner },
    { path: '/ViewsatInetms/:version/:mode/:cusid', component: inetms },
    { path: '/EditsatInetms/:version/:mode/:cusid', component: inetms },
    { path: '/ViewsatNewInetms/:version/:mode/:cusid', component: Newinetms },
    { path: '/EditsatNewInetms/:version/:mode/:cusid', component: Newinetms },
    { path: '*', redirect: '/login' },

    { path: '/Questionnaire', component: Questionnaire },
    { path: '/ViewINETCust.Sat./:version/:mode/:customerID_encoder/:create_by', component: SatisfactionQuestionaire },
    { path: '/EditINETCust.Sat./:version/:mode/:customerID_encoder/:create_by', component: SatisfactionQuestionaire },
    { path: '/NewINETCust.Sat./:customerID_encoder/:mode/:create_by/:link_age', component: SatisfactionQuestionaire },

    { path: '/Followup', component: Talktome },
    { path: '/ViewsatFollowup/:version/:mode/:customerID_encoder/:create_by', component: SatisfactionTalktome },
    { path: '/EditsatFollowup/:version/:mode/:customerID_encoder/:create_by', component: SatisfactionTalktome },
    { path: '/NewINETFollowUp/:customerID_encoder/:mode/:create_by/:link_age', component: SatisfactionTalktome },

    { path: '/DashboardReportExcelQuestionnaire', component: ReportExcelQuestionnaire },
    { path: '/DashboardReportExcelTalktome', component: ReportExcelTalktome },

    { path: '/UserManagement', component: UserManagement },
    { path: '/BackupCustomerDatabase', component: BackupCustomerDatabase },
    { path: '/BackupINETCustSat', component: BackupINETCustSat },
    { path: '/BackupINETFollowUp', component: BackupINETFollowUp }
  ]
})
export default router
